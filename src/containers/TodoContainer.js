import React, {useEffect} from 'react';
import AddTodoForm from "../components/AddTodoForm/AddTodoForm";
import './TodoContainer.css';
import {deleteTodo, fetchTodos} from "../store/actions";
import {useDispatch, useSelector} from "react-redux";
import TodoItem from "../components/TodoItem/TodoItem";
import Backdrop from "../components/UI/Backdrop/Backdrop";
import Spinner from "../components/UI/Spinner/Spinner";

const TodoContainer = () => {
    const dispatch = useDispatch();
    const todos = useSelector(state => state.todos);
    const isLoading = useSelector(state => state.isLoading)

    const deleteTodoItem = id => {
        dispatch(deleteTodo(id));
    }

    useEffect(() => {
        dispatch(fetchTodos());
    }, [dispatch])

    return (
        <>
            <Backdrop show={isLoading}/>
            <div className="TodoContainer">
                <h1>Todos</h1>
                <AddTodoForm/>
                <div className="todo-block">
                    {todos.map(todo => (
                        <TodoItem
                            deleted={() => deleteTodoItem(todo.id)}
                            text={todo.text}
                            key={todo.id}
                        />
                    ))}
                </div>
            </div>
            {isLoading === true ? <Spinner/> : null}
        </>
    );
};

export default TodoContainer;