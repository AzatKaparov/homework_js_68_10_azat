import React from 'react';
import './MainPage.css';
import {NavLink} from "react-router-dom";

const MainPage = () => {
    return (
        <div className="MainPage">
            <NavLink to="/todos">TODOS</NavLink>
            <NavLink to="/counter">Counter</NavLink>
        </div>
    );
};

export default MainPage;