import React from 'react';
import './TodoItem.css';

const TodoItem = ({ text, deleted }) => {
    return (
        <div className="TodoItem">
            <p>{text}</p>
            <button onClick={deleted}>X</button>
        </div>
    );
};

export default TodoItem;