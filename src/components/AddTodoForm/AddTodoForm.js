import React from 'react';
import './AddTodoForm.css'
import {useDispatch, useSelector} from "react-redux";
import {changeCurrentTodoValue, sendTodo} from "../../store/actions";

const AddTodoForm = () => {
    const currentTodoValue = useSelector(state => state.currentTodoValue);
    const dispatch = useDispatch();

    const onFormSubmit = e => {
        e.preventDefault();
        dispatch(sendTodo({text: currentTodoValue}));
    };

    const onValueChange = e => {
        dispatch(changeCurrentTodoValue(e.target.value));
    }

    return (
        <form onSubmit={onFormSubmit} className="AddTodoForm">
            <input
                onChange={onValueChange}
                value={currentTodoValue}
                placeholder="Task text"
                type="text"
                name="todoText"
                id="todoInp"/>
            <button type="submit">SEND</button>
        </form>
    );
};

export default AddTodoForm;