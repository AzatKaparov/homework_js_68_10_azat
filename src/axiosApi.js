import axios from 'axios';

const axiosApi = axios.create({
    baseURL: 'https://lab-68-48c73-default-rtdb.firebaseio.com/'
});

export default axiosApi;