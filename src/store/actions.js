import axiosApi from "../axiosApi";

export const FETCH_TODO_SUCCESS = 'FETCH_TODO_SUCCESS';
export const SEND_TODO_SUCCESS = "SEND_TODO_SUCCESS";
export const CHANGE_CURRENT_TODO_VALUE = "CHANGE_CURRENT_TODO_VALUE";
export const FETCH_ANY_REQUEST = 'FETCH_ANY_REQUEST';
export const FETCH_ANY_ERROR = 'FETCH_ANY_ERROR';
export const RESET_CURRENT_TODO = 'RESET_CURRENT_TODO';
export const DELETE_TODO_SUCCESS = "DELETE_TODO_SUCCESS";

export const fetchAnyRequest = () => ({type: FETCH_ANY_REQUEST});
export const fetchAnyError = () => ({type: FETCH_ANY_ERROR});
export const fetchTodoSuccess = todos => ({type: FETCH_TODO_SUCCESS, todos});
export const sendTodoSuccess = todo => ({type: SEND_TODO_SUCCESS, todo});
export const changeCurrentTodoValue = value => ({type: CHANGE_CURRENT_TODO_VALUE, value});
export const resetCurrentTodoValue = () => ({type: RESET_CURRENT_TODO});
export const deleteTodoSuccess = id => ({type: DELETE_TODO_SUCCESS, id});


export const fetchTodos = () => {
    return async dispatch => {
        dispatch(fetchAnyRequest());

        try {
            const response = await axiosApi.get('./todos.json');
            dispatch(fetchTodoSuccess(response.data));
        } catch (e) {
            dispatch(fetchAnyError())
        }
    }
}

export const sendTodo = toSentTodo => {
    return async dispatch => {
        dispatch(fetchAnyRequest());

        try {
            const response = await axiosApi.post('./todos.json', toSentTodo);
            dispatch(sendTodoSuccess(response.data.name));
            dispatch(resetCurrentTodoValue());
        } catch (e) {
            dispatch(fetchAnyError())
        }
    }
}

export const deleteTodo = id => {
    return async dispatch => {
        dispatch(fetchAnyRequest());

        try {
            await axiosApi.delete(`./todos/${id}.json`);
            dispatch(deleteTodoSuccess(id));
        } catch (e) {
            dispatch(fetchAnyError());
        }
    }
}

// Counter

export const INCREASE = 'INCREASE';
export const DECREASE = 'DECREASE';
export const ADD = 'ADD';
export const SUBTRACT = 'SUBTRACT';

export const FETCH_COUNTER_REQUEST = 'FETCH_COUNTER_REQUEST';
export const FETCH_COUNTER_SUCCESS = 'FETCH_COUNTER_SUCCESS';
export const FETCH_COUNTER_FAILURE = 'FETCH_COUNTER_FAILURE';

export const SAVE_COUNTER_SUCCESS = "SAVE_COUNTER_SUCCESS";

export const saveCounterSuccess = () => ({type: SAVE_COUNTER_SUCCESS});

export const increase = () => ({type: INCREASE});
export const decrease = () => ({type: DECREASE});
export const add = value => ({type: ADD, payload: value});
export const subtract = value => ({type: SUBTRACT, payload: value});

export const fetchCounterSuccess = counter => ({type: FETCH_COUNTER_SUCCESS, payload: counter});

export const fetchCounter = () => {
    return async (dispatch) => {
        dispatch(fetchAnyRequest());

        try {
            const response = await axiosApi.get('./counter.json');

            if (response.data === null) {
                dispatch(fetchCounterSuccess(0));
            } else {
                dispatch(fetchCounterSuccess(response.data));
            }
        } catch (e) {
            dispatch(fetchAnyError());
        }
    };
};

export const saveCounter = () => {
    return async (dispatch, getState) => {
        dispatch(fetchAnyRequest());
        const currentCounter = getState().counter;
        await axiosApi.put('./counter.json', currentCounter);
        dispatch(saveCounterSuccess());
    }
};