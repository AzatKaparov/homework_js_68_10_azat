import {
    ADD,
    CHANGE_CURRENT_TODO_VALUE,
    DECREASE,
    DELETE_TODO_SUCCESS,
    FETCH_ANY_ERROR,
    FETCH_ANY_REQUEST,
    FETCH_COUNTER_FAILURE,
    FETCH_COUNTER_REQUEST,
    FETCH_COUNTER_SUCCESS,
    FETCH_TODO_SUCCESS,
    INCREASE,
    RESET_CURRENT_TODO, SAVE_COUNTER_SUCCESS,
    SEND_TODO_SUCCESS,
    SUBTRACT,
} from "./actions";


const initialState = {
    todos: [],
    isLoading: null,
    currentTodoValue: "",
    counter: 0,
    error: null,
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_ANY_REQUEST:
            return {...state, isLoading: true};
        case FETCH_TODO_SUCCESS:
            const newTodos = Object.keys(action.todos).map(todo => {
                return {...action.todos[todo], id: todo}
            });
            return {...state, todos: newTodos, isLoading: false};
        case SEND_TODO_SUCCESS:
            return {...state, todos: [...state.todos, {id: action.todo, text: state.currentTodoValue}], isLoading: false};
        case FETCH_ANY_ERROR:
            return {...state, isLoading: false};
        case CHANGE_CURRENT_TODO_VALUE:
            return {...state, currentTodoValue: action.value};
        case RESET_CURRENT_TODO:
            return {...state, currentTodoValue: ""};
        case DELETE_TODO_SUCCESS:
            return {...state, todos: state.todos.filter(item => item.id !== action.id), isLoading: false};
        //    Counter
        case INCREASE:
            return {...state, counter: state.counter + 1};
        case DECREASE:
            return {...state, counter: state.counter - 1};
        case ADD:
            return {...state, counter: state.counter + action.payload};
        case SUBTRACT:
            return {...state, counter: state.counter - action.payload};
        case FETCH_COUNTER_REQUEST:
            return {...state, error: null, isLoading: true};
        case FETCH_COUNTER_SUCCESS:
            return {...state, isLoading: false, counter: action.payload};
        case FETCH_COUNTER_FAILURE:
            return {...state, isLoading: false, error: action.payload};
        case SAVE_COUNTER_SUCCESS:
            return {...state, isLoading: false};
        default:
            return state;
    }
};

export default reducer;