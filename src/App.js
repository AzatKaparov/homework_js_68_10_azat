import './App.css';
import TodoContainer from "./containers/TodoContainer";
import {BrowserRouter, Route, Switch} from "react-router-dom";
import MainPage from "./components/MainPage/MainPage";
import Counter from "./components/Counter/Counter";

function App() {
  return (
    <div className="App">
        <BrowserRouter>
            <Switch>
                <Route path="/" exact component={MainPage}/>
                <Route path="/todos" exact component={TodoContainer}/>
                <Route path="/counter" exact component={Counter}/>
                <Route render={() => <h1>Not found</h1>}/>
            </Switch>
        </BrowserRouter>
    </div>
  );
}

export default App;
